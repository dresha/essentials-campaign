/**
 * Maxymiser Core - Campaign
 *
 * Campaign constructor that provides basic functionality for campaign development.
 *
 * @version 0.3.4
 *
 * @requires mmcore.Deferred
 * @requires mmcore.tryCatch
 * @requires mmcore.AttachStyle
 *
 * @author evgeniy.pavlyuk@maxymiser.com (Evgeniy Pavlyuk)
 * @author andrey.ponamarev@maxymiser.com (Andrey Ponamarev)
 * @author aleksey.porokhnya@maxymiser.com (Aleksey Porokhnya)
 */
(function() {
    'use strict';
    /**
     * @type {Campaign}
     */ 
    mmcore.Campaign = Campaign;

    /**
     * @param {*} itemToCheck
     * @param {Array} arr
     * @returns {boolean}
     */
    function inArray(itemToCheck, arr) {
        var inArray = false;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === itemToCheck) {
                inArray = true;
            }
        }
        return inArray;
    }

    /**
     *
     * @param elems
     * @param callback
     * @param inv
     * @returns {Array}
     */
    var grep = function( elems, callback, inv) {
        var retVal,
            ret = [],
            i = 0,
            length = elems.length;
        inv = !!inv;

        // Go through the array, only saving the items
        // that pass the validator function
        for ( ; i < length; i++ ) {
            retVal = !!callback( elems[ i ], i );
            if ( inv !== retVal ) {
                ret.push( elems[ i ] );
            }
        }

        return ret;
    }

  /**
   * Campaign constructor that provides basic functionality for campaign development.   *
   * Documentation https://bitbucket.org/gamingteam/essentials-campaign
   *
   *
   * @name Campaign
   * @namespace
   * @constructor
   * 
   * @param {string} name Campaign name from the UI.
   * @param {!Array.<string>} maxyboxNames Names of campaign elements from the UI.
   * @param {string} prefix Prefix with campaign type and number, for example, mt1.
   */
  function Campaign(name, maxyboxNames, prefix) {
    /**
     * Campaign name.
     * @type {string}
     */
    this.name = name;
    /**
     * Names of campaign elements.
     * @type {!Array.<string>}
     */
    this.maxyboxNames = maxyboxNames;
    /**
     * Campaign type and number prefix.
     * @type {string}
     */
    this.prefix = prefix;

    this.preventDefaultRendering();
    this.preventDefaultHiding();
  }

    /**
     * Marks the campaign elements as rendered.
     */
    Campaign.prototype.preventDefaultRendering = function() {
        /* jshint -W106 */
        var states = mmcore._r_mbs;
        /* jshint +W106 */
        var maxyBoxes = this.maxyboxNames;
        var l = maxyBoxes.length;

        while(l--){
            states[maxyBoxes[l]] = 1;
        }
    };

  /**
   * Excludes the campaign elements from arguments of `mmcore.HideMaxyboxes` when it's called.
   */
  Campaign.prototype.preventDefaultHiding = function() {
      var campaign = this;
      mmcore.HideMaxyboxes = (function(hideMaxyboxes) {
          return function() {
              var maxyboxNames = arguments;

              mmcore.tryCatch(function() {
                  maxyboxNames = grep(maxyboxNames, function(maxyboxName) {
                      return (inArray(maxyboxName, campaign.maxyboxNames));
                  });
              })();

              if (maxyboxNames.length) {
                  return hideMaxyboxes.apply(this, maxyboxNames);
              }
          };
      }(mmcore.HideMaxyboxes));
	  mmcore._MbStyle = function () {};
  };

  /**
   * Hides content by selector.
   *
   * @param {string|Array.<string>} selector CSS selector(s).
   * @param {string=} hidingStyle Optional CSS to be applied to the selector.
   *     If ommited, the content is hidden through absolute positioning.
   */
  Campaign.prototype.hideContent = function(selector, hidingStyle) {
      var hidingClass;
      var hidingSelectorStyle;
      var countSelectors;
      var hidingSelectors;
      var htmlTag = document.getElementsByTagName('html')[0];
      var htmlClass = htmlTag.getAttribute('class');
      var styleStr = '';
      var isEmpty = function(obj) {

          if (obj == null) return true;

          if (obj.length > 0)    return false;
          if (obj.length === 0)  return true;

          for (var key in obj) {
              if (hasOwnProperty.call(obj, key)) return false;
          }

          return true;
      };

      this.hidingClass = this.prefix + '-hidden-content';

      // Add Class to html
      htmlClass = (htmlClass !== null && htmlClass === '') ? this.hidingClass : htmlClass + ' ' + this.hidingClass;

      htmlTag.setAttribute('class', htmlClass);

      if (arguments.length < 2) {
          hidingStyle = 'left: -33554430px; position: absolute; top: -33554430px;';
      }

      hidingClass = this.hidingClass;
      
      if ({}.toString.call(selector) === '[object Object]' && !isEmpty(selector)) {
          for (var key in selector) {
              styleStr += ' .' + hidingClass + ' ' + key + '{' + selector[key] + '}';
          }
          mmcore.AttachStyle(styleStr);
          return;
      }

      hidingSelectors = selector.split(',');
      countSelectors = hidingSelectors.length;
      hidingSelectorStyle = '';

      while(countSelectors--){
          hidingSelectorStyle += '.' + hidingClass + ' ' + hidingSelectors[countSelectors] + '{' +
              hidingStyle +
              '}'
      }
      mmcore.AttachStyle(hidingSelectorStyle);
  };

  /**
   * Shows the content previously hidden by `Campaign.prototype.hideContent`.
   */
  Campaign.prototype.showContent = function() {
      if (this.hasOwnProperty('hidingClass')) {
          var htmlTag = document.getElementsByTagName('html')[0];
          var htmlClass = htmlTag.getAttribute('class');
          if(htmlClass !== null && htmlClass !== ''){
              htmlClass = htmlClass.replace(this.hidingClass, '');
              htmlTag.setAttribute('class', htmlClass);
          }
      }
  };

  /**
   * Returns generated experience of this campaign from GenInfo
   *
   * @returns {Object}
   */
  Campaign.prototype.getExperience = function() {
    // Assume that an own property of mmcore.GenInfo reference to a unique object.
    return mmcore.GenInfo.hasOwnProperty(this.name) ?
        mmcore.GenInfo[this.name] : null;
  };

  /**
   * Returns true if campaign has non default experience
   *
   * @return {boolean}
   */
  Campaign.prototype.hasNonDefaultExperience = function() {
      var experience, hasNonDefaultExperience;
      experience = this.getExperience();
      if (!experience) {
          return false;
      }

      hasNonDefaultExperience = false;

      for(var key in experience){
          if (experience.hasOwnProperty(key) &&
              (experience[key] !== 'Default')) {
              hasNonDefaultExperience = true;
              break;
          }
      }

      return hasNonDefaultExperience;
  };

  /**
   * @return {boolean}
   * @deprecated Use hasNonDefaultExperience() instead.
   */
  Campaign.prototype.hasMaxybox = function() {
    return this.hasNonDefaultExperience();
  };
  
  
	/**
	 * Extending method for campaign
	 * 
	 * @param {Object} obj
	 */
	Campaign.prototype.extend = function extend(obj) {
            for(var prop in obj)
                this[prop] = obj[prop];

            return this;
	},

  /**
   * Render all or specific campaign maxyboxes.
   * this in a variant script references to the campaign.
   */
  Campaign.prototype.renderMaxyboxes = function() {
      var campaign;
      var maxyboxNames;
      var key;

      maxyboxNames = this.maxyboxNames;

      if (arguments.length) {
          maxyboxNames = grep(arguments, function(maxyboxName) {
              return !inArray(maxyboxName, maxyboxNames);
          });
      }

      campaign = this;

      for(key in mmcore._renderers){
          if(inArray(key, maxyboxNames) && typeof mmcore._renderers[key] === 'function'){
              mmcore._renderers[key].call(campaign);
          }
      }
  };
}());
